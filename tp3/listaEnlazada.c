#include <stdio.h>
#include <stdlib.h>

typedef struct estructuraNodo
{
    int valor;
    struct estructuraNodo *proximo;
} Nodo;

Nodo *inicializarLista(int elemento)
{
    Nodo *lista = malloc(sizeof(Nodo));
    lista->valor = elemento;
    return lista;
}

Nodo *aniadirNodo(int elemento, Nodo *lista)
{

    Nodo *nodoActual = lista;
    Nodo *nodoAnterior = NULL;
    Nodo *nuevoNodo = malloc(sizeof(Nodo));
    nuevoNodo->valor = elemento;

    while (nodoActual != NULL)
    {
        if (elemento > nodoActual->valor)
        {
            nodoAnterior = nodoActual;
            nodoActual = nodoActual->proximo;
        }
        else
        {
            if (nodoAnterior == 0x0)
            {
                lista = nuevoNodo;
            }
            else
            {
                nodoAnterior->proximo = nuevoNodo;
            }
            nuevoNodo->proximo = nodoActual;
            break;
        }
    }

    return lista;
}

void *aniadirNodoPunteroPuntero(int elemento, Nodo **lista)
{

    Nodo *nodoActual = *lista;
    Nodo *nodoAnterior = NULL;
    Nodo *nuevoNodo = malloc(sizeof(Nodo));
    nuevoNodo->valor = elemento;

    while (nodoActual != NULL)
    {
        if (elemento > nodoActual->valor)
        {
            nodoAnterior = nodoActual;
            nodoActual = nodoActual->proximo;
        }
        else
        {
            if (nodoAnterior == 0x0)
            {
                *lista = nuevoNodo;
            }
            else
            {
                nodoAnterior->proximo = nuevoNodo;
            }
            nuevoNodo->proximo = nodoActual;
            break;
        }
    }
}

int longitudDeLista(Nodo *lista)
{
    int contador = 0;
    Nodo *nodoActual = lista;
    contador++;
    while (nodoActual->proximo != NULL)
    {
        contador++;
        nodoActual = nodoActual->proximo;
    }

    return contador;
}

void imprimirElementos(Nodo *lista)
{
    Nodo *nodoActual = lista;

    printf("[ ");
    while (nodoActual != NULL)
    {
        printf("%i ", nodoActual->valor);
        nodoActual = nodoActual->proximo;
    }
    printf("] \n");
}

int obtenerNElemento(Nodo *lista, int posicion)
{
    Nodo *nodoActual = lista;
    int posActual = 0;
    while (posActual < posicion)
    {
        posActual++;
        nodoActual = nodoActual->proximo;
    }

    return nodoActual->valor;
}

void eliminarNElemento(Nodo *lista, int posicion)
{
    Nodo *nodoActual = lista;
    int posActual = 0;
    if (posicion == 0)
    {
        nodoActual = nodoActual->proximo;
        *lista = *nodoActual;
        return;
    }

    while (posActual < posicion - 1)
    {
        nodoActual = nodoActual->proximo;
        posActual++;
    }

    nodoActual->proximo = nodoActual->proximo->proximo;
}

int main()
{
    //Iniciamos lista
    Nodo *lista = inicializarLista(6);

    // Aniadimos nodos con elementos
    //lista = aniadirNodo(5, lista);
    //lista = aniadirNodo(4, lista);
    //lista = aniadirNodo(1, lista);
    //lista = aniadirNodo(2, lista);
    //lista = aniadirNodo(8, lista);

    aniadirNodoPunteroPuntero(5, &lista);
    aniadirNodoPunteroPuntero(4, &lista);
    aniadirNodoPunteroPuntero(1, &lista);
    aniadirNodoPunteroPuntero(2, &lista);
    aniadirNodoPunteroPuntero(8, &lista);

    // Imprimimos el tamanio de la lista
    printf("Tamanio de la lista: %i \n", longitudDeLista(lista));

    // Imprimimos todos los elementos ORDENADOS
    imprimirElementos(lista); // Se imprime ordenado: [ 1 2 4 5 6 ]

    // Obtenemos un elemento mediante su posicion
    printf("El elemento en la posicion %i es: %i \n", 2, obtenerNElemento(lista, 2));

    // Eliminamos un elemento de la lista mediante posicion y verificamos
    eliminarNElemento(lista, 2);

    printf("Tamanio de la lista: %i \n", longitudDeLista(lista));
    imprimirElementos(lista);
    printf("El elemento en la posicion %i es: %i \n", 2, obtenerNElemento(lista, 2));
}
