
TP2: Defina una estructura que represente una entidad en la vida real. 
Instancie la estructura en el main, popule los datos e imprímalos. 

Parte 2: intente modularizar la inicialización de la estructura usando funciones. 
¿Qué dificultades o cambios se presentan? ¿A qué cree que se deben? 

Parte 3: Diseñe una estructura que represente una persona que puede tener hasta 2 hijos. 
Con las herramientas vistas hasta ahora ¿qué problemática se presenta en términos de manejo de memoria?


P: ¿Qué dificultades o cambios se presentan? ¿A qué cree que se deben?
R: Hay que pasar los punteros en la función y los valores

P: Con las herramientas vistas hasta ahora ¿qué problemática se presenta en términos de manejo de memoria?
R: Hay que crear un struct autoreferencial con punteros y acceder a la variable contenida en estos y obtener asi el valor