#include <stdio.h>

typedef struct
{
    char nombre[30];
    struct Persona *hijo1;
    struct Persona *hijo2;
} Persona;

int main()
{
    Persona persona1;
    Persona hijo1;
    Persona hijo2;

    strcpy(persona1.nombre, "Carlos");
    strcpy(hijo1.nombre, "David");
    strcpy(hijo2.nombre, "Florencia");

    persona1.hijo1 = &hijo1;
    persona1.hijo2 = &hijo2;

    Persona *punteroHijo1 = persona1.hijo1;
    Persona *punteroHijo2 = persona1.hijo2;

    printf("Nombre de persona1: %s \n", persona1.nombre);
    printf("Nombre de hijo1: %s \n", punteroHijo1->nombre);
    printf("Nombre de hijo2 %s \n", punteroHijo2->nombre);
}