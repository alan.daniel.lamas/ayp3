#include <stdio.h>

typedef struct
{
    int ancho;
    int alto;
} Rectangulo;

void iniciarRectangulo(Rectangulo *punteroARectangulo, int ancho, int alto)
{

    punteroARectangulo->ancho = ancho;
    punteroARectangulo->alto = alto;
}

int main()
{
    Rectangulo rectangulo1;
    rectangulo1.ancho = 10;
    rectangulo1.alto = 8;

    printf("Ancho del rectangulo 1: %d \n", rectangulo1.ancho);
    printf("Alto del rectangulo 1: %d \n", rectangulo1.alto);

    Rectangulo *punteroARectangulo;
    punteroARectangulo = malloc(sizeof(Rectangulo));
    iniciarRectangulo(punteroARectangulo, 12, 15);

    printf("Ancho del rectangulo 2: %d \n", punteroARectangulo->ancho);
    printf("Alto del rectangulo 2: %d \n", punteroARectangulo->alto);
}