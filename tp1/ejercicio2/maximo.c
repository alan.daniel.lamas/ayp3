#include <stdio.h>

int main()
{
    int maximo;
    int arreglo[10] = {2, 5, 7, 9, 3, 10, 11, 7, 8, 1};
    int contador = sizeof(arreglo) / sizeof(arreglo[0]);
    for (int i = 0; i < contador; i++)
    {
        if (maximo < arreglo[i])
        {
            maximo = arreglo[i];
        }
    }
    printf("El maximo es: %i \n", maximo);
}