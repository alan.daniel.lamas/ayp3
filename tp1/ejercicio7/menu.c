#include <stdio.h>

int main()
{
    char entrada[100];

    while (strcmp(entrada, "Salir") != 0)
    {
        printf("Elija una opcion: Op1, Op2, Op3, Salir: ");
        scanf("%s", &entrada);

        printf("Entrada == Op1 \n", entrada == "Op1");

        if (strcmp(entrada, "Op1") == 0)
        {
            printf("Esta es la primera opcion \n");
        }
        if (strcmp(entrada, "Op2") == 0)
        {
            printf("Esta es la segunda opcion \n");
        }
        if (strcmp(entrada, "Op3") == 0)
        {
            printf("Esta es la tercera opcion \n");
        }
    }

    printf("Adios \n");
}