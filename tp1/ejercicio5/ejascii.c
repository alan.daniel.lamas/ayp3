#include <stdio.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "");
    int i;
    i = 128;
    do
    {
        printf("%d %c \n", i, i);
        i++;
    } while (i <= 255);
    return 0;
}