#include <stdio.h>

int main()
{
    int minimo;
    int arreglo[10] = {2, 5, 7, 9, -1, 10, 11, 7, 8, 1};
    int contador = sizeof(arreglo) / sizeof(arreglo[0]);
    for (int i = 0; i < contador; i++)
    {
        if (minimo > arreglo[i])
        {
            minimo = arreglo[i];
        }
    }
    printf("El minimo es: %i \n", minimo);
}