#include <stdio.h>

int main()
{
    int primerNumero;
    int segundoNumero;
    int tercerNumero;

    printf("Ingrese el primer numero \n");
    scanf("%d", &primerNumero);

    printf("Ingrese el segundo numero \n");
    scanf("%d", &segundoNumero);

    printf("Ingrese el tercer numero \n");
    scanf("%d", &tercerNumero);

    double promedio = (double)(primerNumero + segundoNumero + tercerNumero) / 3;

    printf("El promedio de estos numeros es %f \n", promedio);
}